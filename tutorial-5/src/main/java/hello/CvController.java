package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/mycv")
    public String mycv(@RequestParam(name = "visitor", required = false)
                                   String visitor, Model model) {
        model.addAttribute("name", visitor);
        String intro = visitor == null ? "This is my CV ." :
                visitor.substring(0, 1).toUpperCase() + visitor.substring(1) + ", Please hire me .";
        model.addAttribute("intro", intro);
        return "mycv";
    }

}
