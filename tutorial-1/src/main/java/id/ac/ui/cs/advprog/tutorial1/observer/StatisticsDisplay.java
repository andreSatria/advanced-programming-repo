package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private Observable observable;
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        // TODO Complete me!
        observable.addObserver(this);
        this.numReadings = 0;
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            // TODO Complete me!
            WeatherData data = (WeatherData) o;
            float currTemp = data.getTemperature();
            if(currTemp < this.maxTemp ){
                this.minTemp = currTemp;
            }
            if(this.maxTemp < currTemp){
                this.maxTemp = currTemp;
            }
            tempSum += currTemp;
            numReadings++;
            display();




        }
    }
}
