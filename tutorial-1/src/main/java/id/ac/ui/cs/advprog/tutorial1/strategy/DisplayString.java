package id.ac.ui.cs.advprog.tutorial1.strategy;

public class DisplayString implements DisplayMethod {
    @Override
    public void display() {
        System.out.println("Displaying as String");
    }
}
