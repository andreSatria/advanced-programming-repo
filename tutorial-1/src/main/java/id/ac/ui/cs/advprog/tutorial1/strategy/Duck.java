package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;
    private SwimBehavior swimBehavior;

    public void performFly() {
        flyBehavior.fly();
    }
    public void performQuack() {
        quackBehavior.quack();
    }
    public void swim() {
        swimBehavior.swim();
    }

    public abstract void display();

    // TODO Complete me!
    public void setFlyBehavior(FlyBehavior update){
        flyBehavior = update;
    }

    public void setQuackBehavior(QuackBehavior update){
        quackBehavior = update;
    }

    public void setSwimBehavior(SwimBehavior update){
        swimBehavior = update;
    }

}
