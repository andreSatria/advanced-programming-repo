package id.ac.ui.cs.advprog.tutorial1.strategy;

public class Swimming implements SwimBehavior{
    @Override
    public void swim() {
        System.out.println("I am Swimming...");
    }
}
