package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    public MallardDuck(){
        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new Quack());
        setSwimBehavior(new Swimming());
    }

    @Override
    public void display() {
        System.out.println("Im a Mallard Duck");
    }
}
