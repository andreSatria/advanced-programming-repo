package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior{
    @Override
    public void fly() {
        System.out.println("Whoops I can't Fly");
    }
    // TODO Complete me!
}
