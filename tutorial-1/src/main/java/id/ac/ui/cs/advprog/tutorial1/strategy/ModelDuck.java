package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    // TODO Complete me!
    public ModelDuck(){
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());
        setSwimBehavior(new CantSwim());
    }

    @Override
    public void display() {
        System.out.println("Im a Mallard Duck");
    }
}
