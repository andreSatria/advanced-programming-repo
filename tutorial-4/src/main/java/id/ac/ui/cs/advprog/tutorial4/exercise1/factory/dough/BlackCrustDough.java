package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class BlackCrustDough implements Dough {
    public String toString() {
        return "Black Crust dough";
    }
}
