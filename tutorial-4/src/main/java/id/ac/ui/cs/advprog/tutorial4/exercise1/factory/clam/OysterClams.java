package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class OysterClams implements Clams {
    @Override
    public String toString() {
        return "Oyster Clams imported from Maine";
    }
}
