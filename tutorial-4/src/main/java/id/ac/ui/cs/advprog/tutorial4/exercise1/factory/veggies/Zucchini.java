package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Zucchini implements Veggies {
    @Override
    public String toString() {
        return "Zucchini";
    }
}
