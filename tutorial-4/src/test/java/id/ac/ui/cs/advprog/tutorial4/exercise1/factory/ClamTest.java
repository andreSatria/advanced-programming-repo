package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;

public class ClamTest {
    private FreshClams freshClams;
    private FrozenClams frozenClams;
    private OysterClams oysterClams;

    @Before
    public void setUp() {
        freshClams = new FreshClams();
        frozenClams = new FrozenClams();
        oysterClams = new OysterClams();

    }

    @Test
    public void testMethodToString() {
        assertEquals("Fresh Clams from Long Island Sound", freshClams.toString());
        assertEquals("Frozen Clams from Chesapeake Bay", frozenClams.toString());
        assertEquals("Oyster Clams imported from Maine", oysterClams.toString());
    }
}
