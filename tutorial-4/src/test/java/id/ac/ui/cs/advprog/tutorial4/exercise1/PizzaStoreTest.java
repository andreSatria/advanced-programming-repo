package id.ac.ui.cs.advprog.tutorial4.exercise1;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class PizzaStoreTest {

    private Class<?> newYorkPizzaStore;
    private Class<?> depokPizzaStore;
    private Class<?> pizzaStore;

    @Before
    public void setUp() throws Exception {
        newYorkPizzaStore = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore");
        depokPizzaStore = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore");
        pizzaStore = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore");
    }

    @Test
    public void testPizzaStoreIsAbstract() {
        int classModifiers = pizzaStore.getModifiers();
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void hasCreatePizzaMethod() throws Exception {
        Method createPizza = pizzaStore.getDeclaredMethod("createPizza", String.class);
        int methodModifiers = createPizza.getModifiers();
        assertTrue(Modifier.isProtected(methodModifiers));
    }

    @Test
    public void testNewYorkPizzaStore() throws Exception {
        Class<?> parent = newYorkPizzaStore.getSuperclass();
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore", parent.getName());
    }

    @Test
    public void testDepokPizzaStore() throws Exception {
        Class<?> parent = depokPizzaStore.getSuperclass();
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore", parent.getName());
    }

    @Test
    public void testNewYorkPizzaStoreOverride() throws Exception {
        Method createPizza = newYorkPizzaStore.getDeclaredMethod("createPizza", String.class);
        int methodModifiers = createPizza.getModifiers();
        assertTrue(Modifier.isProtected(methodModifiers));
    }

    @Test
    public void testDepokPizzaStoreOverride() throws Exception {
        Method createPizza = depokPizzaStore.getDeclaredMethod("createPizza", String.class);
        int methodModifiers = createPizza.getModifiers();
        assertTrue(Modifier.isProtected(methodModifiers));
    }

}
