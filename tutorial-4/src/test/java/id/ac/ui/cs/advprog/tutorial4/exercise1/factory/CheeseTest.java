package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;


public class CheeseTest {
    private ReggianoCheese reggianoCheese;
    private MozzarellaCheese mozzarellaCheese;
    private ProvoloneCheese provoloneCheese;
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() {
        reggianoCheese = new ReggianoCheese();
        mozzarellaCheese = new MozzarellaCheese();
        provoloneCheese = new ProvoloneCheese();
        parmesanCheese = new ParmesanCheese();

    }


    @Test
    public void testMethodToString() {
        assertEquals("Reggiano Cheese", reggianoCheese.toString());
        assertEquals("Shredded Mozzarella", mozzarellaCheese.toString());
        assertEquals("Provolone cheese", provoloneCheese.toString());
        assertEquals("Shredded Parmesan", parmesanCheese.toString());
    }


}
