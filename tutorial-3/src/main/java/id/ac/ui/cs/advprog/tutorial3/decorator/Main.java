package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        Food food = null;
        food = BreadProducer.THICK_BUN.createBreadToBeFilled();
        food = FillingDecorator.BEEF_MEAT.addFillingToBread(food);
        food = FillingDecorator.CHEESE.addFillingToBread(food);

        System.out.println("Made \"" + food.getDescription() + "\" with cost: $" + food.cost());
    }
}
