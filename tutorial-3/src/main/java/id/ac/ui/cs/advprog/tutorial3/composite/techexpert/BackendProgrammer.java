package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    //TODO Implement
    public BackendProgrammer(String name, double salary) {
        if (salary < 20000.00) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.role = "Back End Programmer";
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

}
