package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        if (salary < 100000.00) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.role = "CTO";
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
