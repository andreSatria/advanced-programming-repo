package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;


public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployee(new Ceo("Luffy", 500000));
        try {
            SecurityExpert chopper = new SecurityExpert("Chopper", 10000.0);
            company.addEmployee(chopper);
        } catch (IllegalArgumentException e) {
            System.out.println("Salary is too low");
        }

        System.out.println("Employee List: ");
        for (Employees employee : company.getAllEmployees()) {
            System.out.printf("%s with role %s and salary %f is added to the company!\n",
                    employee.getName(),
                    employee.getRole(),
                    employee.getSalary());
        }
    }
}
